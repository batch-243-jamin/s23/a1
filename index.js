// console.log("Hello World!");

const Trainer = {
		// Properties Pokemon
		name: "Harold Jamin",
		age: 25,
		pokemon: ['Hunter', 'Snorlax', 'Ditto', 'Psyduck'],
		friends: {
			hoenn: ['May', 'Max'], 
			kanto: ['Brock', "Misty"]
		},
		talk: function(){
			console.log(this.pokemon[0] + "! I choose you!")
		}
	}

		console.log(Trainer);
		console.log("Result of dot notation:");
		console.log(Trainer.name);
		console.log("Result of square bracket notation:");
		console.log(Trainer.pokemon);
		console.log("Result of talk method:");
		Trainer.talk();

		function Pokemon(name, level){
		// Properties Pokemon
		this.pokemonName = name;
		this.pokemonLevel = level;
		this.pokemonHealth = 2 * level;
		this.pokemonAttack = level;


		this.tackle = function(targetPokemon){
		remainingHealth = targetPokemon.pokemonHealth - this.pokemonAttack; 
		console.log(this.pokemonName + " tackles " + targetPokemon.pokemonName);
		console.log(targetPokemon.pokemonName + " is now reduced to " + remainingHealth);
		
		targetPokemon.pokemonHealth -= this.pokemonAttack;			
		if (targetPokemon.pokemonHealth <= 0)
			this.fainted(targetPokemon.pokemonName);
		};
		
		
		this.fainted = function(targetPokemon){
			console.log(targetPokemon + " fainted!");
		};

	}
		
		let Hunter = new Pokemon(Trainer.pokemon[0], 30);
		console.log(Hunter);
		let Snorlax = new Pokemon("Snorlax", 50);
		console.log(Snorlax);

		Hunter.tackle(Snorlax);
		Hunter.tackle(Snorlax);
		Hunter.tackle(Snorlax);
		Hunter.tackle(Snorlax);
